const salaryInput= document.querySelector("#input-salary");
const reasonInput= document.querySelector("#input-reason");
const amountInput= document.querySelector("#input-amount");
const addBtn= document.querySelector("#btn-add");
const clearBtn= document.querySelector("#btn-clear");
const expensesList= document.querySelector("#expenses-list");
const totalExpensesOut= document.querySelector("#total-expenses");
const totalMoneyAva= document.querySelector("#available");
const alertController= document.querySelector('ion-alert-controller');

let totalExpenses=0;
let available=  0;

const clear =() => 
{
    reasonInput.value="";
    amountInput.value="";
}


addBtn.addEventListener('click', ()=>
{
    const enteredReason = reasonInput.value;
    const enteredAmount= amountInput.value;

    if(enteredReason.trim().length <=0||
    enteredAmount<= 0 || 
    enteredAmount.trim().length <=0)
    {
        //alert('invalid values!');
        alertController.create({message: 'Please enter a valid reason and amount!', header:'Error', buttons:['Okay']}).then(alertElement => {alertElement.present();});
        return;
    }

    const newItem= document.createElement('ion-item');
    newItem.textContent = enteredReason + ': $' + enteredAmount;

    expensesList.appendChild(newItem);
    
    totalExpenses += +enteredAmount;
    available = +salaryInput.value-totalExpenses;
    totalExpensesOut.textContent='$'+totalExpenses;
    totalMoneyAva.textContent='$'+available;
    clear();

});

clearBtn.addEventListener('click', clear);